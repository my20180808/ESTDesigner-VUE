// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import 'xe-utils'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './plugins/table'

import 'element-ui/lib/theme-chalk/index.css';

import 'jquery'
import 'jquery-migrate/dist/jquery-migrate.js'
import 'jqueryui/jquery-ui.js'

import 'draw2d/dist/draw2d'



import VueHighlightJS from 'vue-highlightjs'
import 'highlight.js/styles/atom-one-dark.css'

import {
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Collapse,
  CollapseItem,
  Button,
  Row,
  Col,
  Card,
  Tabs,
  TabPane,
  Image,
  Form,
  FormItem,
  Input,
  Dialog,
  Select,
  Option,
  Radio,
  RadioGroup,
  Checkbox,
  ButtonGroup
} from 'element-ui';

// import {
//   VXETable,
//   Table,
//   Column,
//   Cell,
//   Header as VXEHeader,
//   Body,
//   Footer as VXEFooter,
//   Icon,
//   Filter,
//   Loading,
//   Tooltip,
//   Grid,
//   Menu,
//   Toolbar,
//   Pager,
//   Checkbox as VXECHeckbox,
//   Radio as VXERadio,
//   Input as VXEInput,
//   Button as VXEButton,
//   Message,
//   Export,
//   Resize
// } from 'vxe-table'
import zhCNLocat from 'vxe-table/lib/locale/lang/zh-CN'
import VXETablePluginElement from 'vxe-table-plugin-element'

import Mock from './mock'

import VXETable from 'vxe-table'
// import 'vxe-table/lib/index.css'

Mock.init();

Vue.config.productionTip = false


Vue.use(Container);
Vue.use(Header);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Footer);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Button);
Vue.use(Row);
Vue.use(Col);
Vue.use(Card);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Image);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Dialog);
Vue.use(Select);
Vue.use(Option);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(Checkbox);
Vue.use(ButtonGroup);

// Vue.use(Table)
// Vue.use(Column)
// Vue.use(Cell)
// Vue.use(VXEHeader)
// Vue.use(Body)
// Vue.use(VXEFooter)
// Vue.use(Icon)
// Vue.use(Filter)
// Vue.use(Loading)
// Vue.use(Tooltip)
// Vue.use(Grid)
// Vue.use(Menu)
// Vue.use(Toolbar)
// Vue.use(Pager)
// Vue.use(VXECHeckbox)
// Vue.use(VXERadio)
// Vue.use(VXEInput)
// Vue.use(VXEButton)
// // Vue.use(Message)
// Vue.use(Export)
// Vue.use(Resize)

// 按需加载的方式默认是不带国际化的，需要自行导入
// VXETable.setup({
//   i18n: (key, value) => VXETable.t(zhCNLocat, key)
// })

//全局默认参数
// Vue.use(VXETable, {
//   size: 'small',
//   tooltipConfig: {
//     zIndex: 3000
//   }
// })
// VXETable.use(VXETablePluginElement)

Vue.use(VueHighlightJS)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
