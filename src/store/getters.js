export const processModel = state => state.process

export const selectedTask = state => state.selectedTask

export const processVariable = state => state.process.variables

export const processDataObj = state => state.process.dataObjects

export const processListeners = state => state.process.listeners

export const taskFormProps = state => state.selectedTask.formProperties

export const taskListeners = state => state.selectedTask.taskListeners

export const executionListeners = state => state.selectedTask.listeners

export const candidateGroups = state => state.selectedTask.candidateGroups

export const candidateUsers = state => state.selectedTask.candidateUsers

export const selectedUsers = state => state.selectedUsers

export const selectedUserGroups = state => state.selectedUserGroups

export const connectionListeners = state => state.selectedConnection.listeners
