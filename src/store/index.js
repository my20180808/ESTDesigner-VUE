import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'
import http from './http'

Vue.use(Vuex);

const state = {
  canvas: {},
  process: {},
  selectedTask: {},
  selectedUsers: [],
  selectedUserGroups: [],
  selectedConnection: {}
}

const store = new Vuex.Store({
  state,
  actions,
  getters,
  mutations,
  modules: {
    http: http
  }
})

export default store
